# python_workout

## Introduction

This is my implementation of the problems in the book [Python Workout](https://www.manning.com/books/python-workout)

## Progress

- [ ] Numeric Types
- [ ] Strings
- [ ] Lists and Tuples
- [ ] Dictionaries and Sets
- [ ] Fles
- [ ] Functions
- [ ] Functional Programming with Comprehensions
- [ ] Modules and Packages
- [ ] Objects
- [ ] Iterators and Generators
