#!/usr/bin/env python3
"""Solution to exercise 01: Guessing Game"""

import random


def guessing_game() -> None:
    """Ask the user to guess a random number between 1 and 100."""

    answer = random.randint(1, 100)

    while True:
        user_guess = int(input('What is your guess? '))

        if user_guess == answer:
            print(f'Right!  The answer is {user_guess}')
            break

        if user_guess < answer:
            print(f'Your guess of {user_guess} is too low!')
        else:
            print(f'Your guess of {user_guess} is too high!')


guessing_game()
